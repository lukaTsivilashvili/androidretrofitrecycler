package com.example.appretrofit

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class InfoViewModel : ViewModel() {

    private val countryLiveData = MutableLiveData<List<CountryModel>>().apply {
        mutableListOf<CountryModel>()
    }

    val _countryLiveData: LiveData<List<CountryModel>> = countryLiveData

    private val loadingLiveData = MutableLiveData<Boolean>()
    val _loadingLiveData:LiveData<Boolean> = loadingLiveData.apply {
        true
    }

    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }
    }


    private suspend fun getCountries() {
        loadingLiveData.postValue(true)
        val result = RetrofitService.service().getCountry()

        if (result.isSuccessful) {
            val items = result.body()
            countryLiveData.postValue(items)
        }
        loadingLiveData.postValue(false)
    }

}