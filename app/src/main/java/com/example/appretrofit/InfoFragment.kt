package com.example.appretrofit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appretrofit.databinding.InfoFragmentBinding

class InfoFragment : Fragment() {

    private val viewModel: InfoViewModel by viewModels()
    private lateinit var binding: InfoFragmentBinding

    private lateinit var myAdapter: RecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InfoFragmentBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        viewModel.init()
        initRecycler()
        observe()

        binding.refresh.setOnRefreshListener {
            myAdapter.clearData()
            viewModel.init()
        }
    }


    private fun initRecycler() {
        myAdapter = RecyclerAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(requireActivity())
        binding.recycler.adapter = myAdapter
    }

    private fun observe() {
        viewModel._loadingLiveData.observe(viewLifecycleOwner, {
            binding.refresh.isRefreshing = it
        })

        viewModel._countryLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.toMutableList())
        })
    }

}