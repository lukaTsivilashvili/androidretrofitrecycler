package com.example.appretrofit

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepo {
    @GET("rest/v2/all")
    suspend fun getCountry(): Response<List<CountryModel>>
}