package com.example.appretrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.example.appretrofit.databinding.CountriesLayoutBinding

class RecyclerAdapter() :
    RecyclerView.Adapter<RecyclerAdapter.ItemViewHolder>() {
    val country = mutableListOf<CountryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView =
            CountriesLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = country.size

    inner class ItemViewHolder(private val binding: CountriesLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: CountryModel

        fun bind() {
            model = country[adapterPosition]

            binding.TVcountry.text = model.name
            binding.TVregion.text = model.region
            binding.TVnativeName.text = model.nativeName
            binding.IVjson.loadSvg(model.flag)
        }
    }

    fun setData(country:MutableList<CountryModel>){
        this.country.addAll(country)
        notifyDataSetChanged()
    }

    fun clearData(){
        this.country.clear()
        notifyDataSetChanged()
    }


    fun ImageView.loadSvg(url: String) {

        val imageLoader = ImageLoader.Builder(this.context)
            .componentRegistry { add(SvgDecoder(this@loadSvg.context)) }
            .build()

        val request = ImageRequest.Builder(this.context)
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .data(url)
            .target(this)
            .build()

        imageLoader.enqueue(request)
    }
}
